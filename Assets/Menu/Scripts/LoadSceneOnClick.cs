﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneOnClick : MonoBehaviour {
    
    public void LoadByIndex(int sceneIndex)
    {
        if (sceneIndex == 0)
        {
            GameParameters.getInstance().Score = 0;
            GameParameters.getInstance().LevelNumber = 0;
        }
        SceneManager.LoadScene(sceneIndex);
    }

    public void nextScene()
    {
        int nextScene = SceneManager.GetActiveScene().buildIndex + 1;
        if (nextScene < SceneManager.sceneCountInBuildSettings)
        {
            GameParameters.getInstance().LevelNumber++;
            if (GameParameters.getInstance().LevelNumber == 4)
            {
                GameParameters.getInstance().Score = 0;
                if (GameParameters.getInstance().Hero is Warrior)
                {
                    GameParameters.getInstance().Hero = new Warrior();
                }
                else if (GameParameters.getInstance().Hero is Monk)
                {
                    GameParameters.getInstance().Hero = new Monk();
                }
                else
                {
                    GameParameters.getInstance().Hero = new Magician();
                }
                GameParameters.getInstance().Difficulty = GameParameters.getInstance().Difficulty;
            }
            SceneManager.LoadScene(nextScene);
        }
        else
        {
            GameParameters.getInstance().LevelNumber++;
            SceneManager.LoadScene(4);
        }
    }
    
    public void skipTutorial()
    {
        GameParameters.getInstance().LevelNumber = 4;
        SceneManager.LoadScene(4);
    }

    public void reloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
