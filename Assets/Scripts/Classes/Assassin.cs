﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Assassin : Mobile
{

    // Attributs

    // Constructors
    public Assassin() : base(40, 40, 8, new Closed(100, 1.0f, 1), false, new Position(), 5.0f)
    {
        
    }

    public Assassin(int totalHealthValue, int remainingHealthValue, int experienceValue, Weapon weaponRef, bool indestructibleValue, Position positionRef, float movementSpeedValue) :
        base(totalHealthValue, remainingHealthValue, experienceValue, weaponRef, indestructibleValue, positionRef, movementSpeedValue)
    {
        
    }

    // Properties

}
