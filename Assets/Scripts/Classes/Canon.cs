﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canon : Fixed
{

    // Attributs

    // Constructors
    public Canon() : base(60, 60, 0, new Ranged(75, 1.0f, 1.0f, 0.0f), true, null)
    {
    }

    public Canon(int totalHealthValue, int remainingHealthValue, int experienceValue, Weapon weaponRef, bool indestructibleValue, Position positionRef) :
        base(totalHealthValue, remainingHealthValue, experienceValue, weaponRef, indestructibleValue, positionRef)
    {

    }

    // Properties

}
