﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : Fixed
{

    // Attributs
    public int potion;

    // Constructors

    public Chest() : base(1, 1, 0, null, false, null)
    {
        potion = Mathf.FloorToInt(Random.Range(0,3));
        Debug.Log(potion);
    }

    public Chest(int totalHealthValue, int remainingHealthValue, int experienceValue, Weapon weaponRef, bool indestructibleValue, Position positionRef, int potionRef) :
        base(totalHealthValue, remainingHealthValue, experienceValue, weaponRef, indestructibleValue, positionRef)
    {
        potion =potionRef%2;
    }

}
