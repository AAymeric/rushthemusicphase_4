﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Closed : Weapon
{

    // Attributs
    private float range;

    // Constructors
    public Closed() : base()
    {
        range = 1;
    }

    public Closed(int damageValue, float attackSpeedValue, int rangeValue) : base(damageValue, attackSpeedValue)
    {
        range = rangeValue;
    }

    // Properties
    public float Range { get { return range; } set { range = value; } }

}
