﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Difficulty
{

    // Attributs
    private string titre;
    private float multiplicateur;

    // Constructors
    public Difficulty(float multiplicateurValue, string titreValue)
    {
        multiplicateur = multiplicateurValue;
        titre = titreValue;
    }

    // Properties
    public float Multiplicateur { get { return multiplicateur; } set { multiplicateur = value; } }
    public string Titre { get { return titre; } set { titre = value; } }

}
