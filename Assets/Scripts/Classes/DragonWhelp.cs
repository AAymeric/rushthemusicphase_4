﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonWhelp : Mobile
{

    // Attributs

    // Constructors
    public DragonWhelp() : base(30, 30, 5, new Ranged(15, 1.0f, 1.0f, 1.0f), false, null, -5.0f)
    {

    }

    public DragonWhelp(int totalHealthValue, int remainingHealthValue, int experienceValue, Weapon weaponRef, bool indestructibleValue, Position positionRef, float movementSpeedValue) :
        base(totalHealthValue, remainingHealthValue, experienceValue, weaponRef, indestructibleValue, positionRef, movementSpeedValue)
    {

    }

    // Properties
    
}
