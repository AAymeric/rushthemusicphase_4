﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Easy : Difficulty
{

    // Constructors
    public Easy() : base(0.8F, "Easy")
    {

    }

}
