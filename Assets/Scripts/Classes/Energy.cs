﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Energy : Potion
{

    // Attributs
    private int amount;

    // Constructors
    public Energy() : base()
    {
        amount = 10;
    }

    public Energy(float timeEffectValue, int amountValue) : base(timeEffectValue)
    {
        amount = amountValue;
    }

    // Properties
    public int Amount { get { return amount; } set { amount = value; } }

}
