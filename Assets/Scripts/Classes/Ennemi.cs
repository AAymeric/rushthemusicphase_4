﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ennemi : Component
{

    // Attributs

    private int totalHealth;
    private int remainingHealth;
    private int experience;
    private Weapon weapon;
    private bool indestructible;
    private Position position;

    // Constructors
    public Ennemi(int totalHealthValue, int remainingHealthValue, int experienceValue, Weapon weaponRef, bool indestructibleValue, Position positionRef)
    {
        totalHealth = totalHealthValue;
        remainingHealth = remainingHealthValue;
        experience = experienceValue;
        Weapon = weaponRef;
        indestructible = indestructibleValue;
        position = positionRef;
        movementSpeed = 5;
    }

    // Properties

    public int TotalHealth { get { return totalHealth; } set { totalHealth = value; } }
    public int RemainingHealth { get { return remainingHealth; } set { remainingHealth = value; } }
    public int Experience { get { return experience; } set { experience = value; } }
    public Weapon Weapon { get { return weapon; } set { weapon = value; } }
    public bool Indestructible { get { return indestructible; } set { indestructible = value; } }
    public Position Position { get { return position; } set { position = value; } }
    public float movementSpeed { get; set; }

    // Others functions

    public void takeDamage(int damage)
    {
        if (!Indestructible)
        {
            RemainingHealth -= damage;
        }
    }

    public void death()
    {
    }
}
