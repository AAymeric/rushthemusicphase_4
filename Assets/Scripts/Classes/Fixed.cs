﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Fixed : Ennemi
{

    // Attributs

    // Constructors
    public Fixed(int totalHealthValue, int remainingHealthValue, int experienceValue, Weapon weaponRef, bool indestructibleValue, Position positionRef) : 
        base(totalHealthValue, remainingHealthValue, experienceValue, weaponRef, indestructibleValue, positionRef)
    {

    }

}
