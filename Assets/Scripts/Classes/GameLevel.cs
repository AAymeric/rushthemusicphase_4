﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLevel
{

    // Attributs
    private int id;
    private Music music;
    private Dictionary<Ennemi, Position> ennemies;

    // Constructors
    public GameLevel(int idValue, Music musicRef, Dictionary<Ennemi, Position> ennemiesRef)
    {
        id = idValue;
        music = musicRef;
        ennemies = ennemiesRef;
    }

    // Properties
    public int Id { get { return id; } set { id = value; } }
    public Music Music { get { return music; } set { music = value; } }
    public Dictionary<Ennemi, Position> Ennemies { get { return ennemies; } set { ennemies = value; } }

}
