﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameParameters
{

    private static GameParameters instance = null;
    private enum Hand { Right, Left }

    // Attributs
    private Hero hero;
    private Difficulty difficulty;
    private int mainHand;
    private Music music;
    private String name;
    private int levelNumber;
    private int score;
    
    public static GameParameters getInstance()
    {
        if (instance == null)
        {
            instance = new GameParameters();
        }
        return instance;
    }

    // Constructeurs par défaut 
    private GameParameters()
    {
        this.hero        = new Warrior();
        this.difficulty  = new Medium();
        this.mainHand    = (int)Hand.Right;
        this.music       = new Music();
        this.name        = "Player";
        this.levelNumber = 1;
        this.score       = 0;
    }

    // Properties
    public Hero Hero { get { return hero; } set { hero = value; } }
    public int MainHand { get { return mainHand; } set { mainHand = value; } }
    public String Name { get { return name; } set { name = value; } }
    public int LevelNumber { get { return levelNumber; } set { levelNumber = value; } }
    public int Score { get { return score; } set { score = value; } }

    public Difficulty Difficulty
    {
        get
        {
            return difficulty;
        }
        set
        {
            difficulty = value;
            if (value is Easy)
            {
                this.hero.easyMode();
            }
            else if (value is Hard)
            {
                this.hero.hardMode();
            }
        }
    }

    public void save()
    {
        PlayerPrefs.SetString("name", this.name);
        PlayerPrefs.SetString("difficulty", this.difficulty.Titre);
        PlayerPrefs.SetInt("mainHand", this.mainHand);
        if (this.levelNumber % 3 == 0)
        {
            this.levelNumber = 3;
        }
        else
        {
            this.levelNumber = this.levelNumber % 3;
        }
        PlayerPrefs.SetInt("levelNumber", this.levelNumber);
        PlayerPrefs.SetInt("score", this.score);
        
        if (this.hero is Warrior)
        {
           PlayerPrefs.SetString("heroClass", "Warrior"); 
        }
        else if (this.hero is Monk)
        {
            PlayerPrefs.SetString("heroClass", "Monk"); 
        }
        else
        {
            PlayerPrefs.SetString("heroClass", "Magician"); 
        }
        
        PlayerPrefs.SetInt("heroExperience", this.hero.Experience);
        PlayerPrefs.SetInt("lifePotion", this.hero.Inventory[0]);
        PlayerPrefs.SetInt("energyPotion", this.hero.Inventory[1]);
        PlayerPrefs.SetInt("invulnerabilityPotion", this.hero.Inventory[2]);
        
        this.saveScore();
    }

    public void saveScore()
    {
        if (this.score > PlayerPrefs.GetInt("score10"))
        {
            PlayerPrefs.SetInt("score10", this.score);
            PlayerPrefs.SetString("name10", this.name);

            for (int i = 9; i > 0; i--)
            {
                if (this.score > PlayerPrefs.GetInt("score" + i))
                {
                    PlayerPrefs.SetInt("score" + (i + 1), PlayerPrefs.GetInt("score" + i));
                    PlayerPrefs.SetString("name" + (i + 1), PlayerPrefs.GetString("name" + i));
                    
                    PlayerPrefs.SetInt("score" + i, this.score);
                    PlayerPrefs.SetString("name" + i, this.name);
                }
            }
        }
        
        PlayerPrefs.SetInt("HighScore", PlayerPrefs.GetInt("score1"));
    }

    public void restaureSave()
    {
        String stringDifficulty;
        String stringClass;
        
        this.name        = PlayerPrefs.GetString("name", "Player");
        this.mainHand    = PlayerPrefs.GetInt("mainHand", 0);
        this.levelNumber = PlayerPrefs.GetInt("levelNumber", 1);
        this.score       = PlayerPrefs.GetInt("score", 0); 
        
        stringClass = PlayerPrefs.GetString("heroClass", "Warrior");
        if (stringClass.Equals("Warrior"))
        {
            this.hero = new Warrior();
        }
        else if (stringClass.Equals("Monk"))
        {
            this.hero = new Monk();
        }
        else
        {
            this.hero = new Magician();
        }
        
        stringDifficulty =  PlayerPrefs.GetString("difficulty", "Medium");
        if (stringDifficulty.Equals("Easy"))
        {
            Difficulty = new Easy();
        }
        else if (stringDifficulty.Equals("Medium"))
        {
            Difficulty = new Medium();
        }
        else
        {
            Difficulty = new Hard();
        }
        
        this.hero.Experience += PlayerPrefs.GetInt("heroExperience", 0);
        while (this.hero.Experience > this.hero.NextLevel && this.hero.Level < 5)
        {
            this.hero.levelUp();
        }
        
        this.hero.Inventory[0] = PlayerPrefs.GetInt("lifePotion", 0);
        this.hero.Inventory[1] = PlayerPrefs.GetInt("energyPotion", 0);
        this.hero.Inventory[2] = PlayerPrefs.GetInt("invulnerabilityPotion", 0);
    }
}
