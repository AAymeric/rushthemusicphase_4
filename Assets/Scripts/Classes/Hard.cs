﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hard : Difficulty
{

    // Constructors
    public Hard() : base(1.2F, "Hard")
    {

    }

}
