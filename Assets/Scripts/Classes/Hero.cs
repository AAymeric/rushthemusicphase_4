﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

public abstract class Hero
{

    // Attributs
    protected int maxHealth;
    protected int currentHealth;
    protected float force;
    protected int experience;
    protected int level;
    protected int nextLevel;
    protected int maxEnergy;
    protected int currentEnergy;
    protected int[] inventory;
    protected float hpRegeneration;
    protected Weapon weapon;
    protected string image;
    protected int energyRegeneration;
   
    // Constructors
    // Base conctructor
    public Hero(int maxHealthValue, int currentHealthValue, float forceValue, int maxEnergyValue, int currentEnergyValue, float hpRegenerationValue, Weapon weaponRef, string imagePath, int energyRegenerationValue)
    {
        maxHealth = maxHealthValue;
        currentHealth = currentHealthValue;
        force = forceValue;
        experience = 0;
        level = 0;
        nextLevel = 10;
        maxEnergy = maxEnergyValue;
        currentEnergy = currentEnergyValue;
        inventory = new int[3];
        inventory[0] = 0;
        inventory[1] = 0;
        inventory[2] = 0;
        hpRegeneration = hpRegenerationValue;
        weapon = weaponRef;
        image = imagePath;
        energyRegeneration = energyRegenerationValue;
    }

    // Custom level contructor
    public Hero(int maxHealthValue, int currentHealthValue, float forceValue, int experienceValue, int levelValue, int nextLevelValue, int maxEnergyValue, int currentEnergyValue, float hpRegenerationValue, Weapon weaponRef, string imagePath, int energyRegenerationValue)
    {
        maxHealth = maxHealthValue;
        currentHealth = currentHealthValue;
        force = forceValue;
        experience = experienceValue;
        level = levelValue;
        nextLevel = nextLevelValue;
        maxEnergy = maxEnergyValue;
        currentEnergy = currentEnergyValue;
        inventory = new int[3];
        inventory[0] = 0;
        inventory[1] = 0;
        inventory[2] = 0;
        hpRegeneration = hpRegenerationValue;
        weapon = weaponRef;
        image = imagePath;
        energyRegeneration = energyRegenerationValue;
    }

    // Properties
    public int MaxHealth { get { return maxHealth; } set { maxHealth = value; } }
    public int CurrentHealth { get { return currentHealth; } set { currentHealth = value; } }
    public float Force { get { return force; } set { force = value; } }
    public int Experience { get { return experience; } set { experience = value; } }
    public int Level { get { return level; } set { level = value; } }
    public int NextLevel { get { return nextLevel; } set { nextLevel = value; } }
    public int MaxEnergy { get { return maxEnergy; } set { maxEnergy = value; } }
    public int CurrentEnergy { get { return currentEnergy; } set { currentEnergy = value; } }
    public int[] Inventory { get { return inventory; } set { inventory = value; } }
    public float HpRegeneration { get { return hpRegeneration; } set { hpRegeneration = value; } }
    public Weapon Weapon { get { return weapon; } set { weapon = value; } }
    public string Image { get { return image; } set { image = value; } }
    public int EnergyRegeneration { get { return energyRegeneration; } set { energyRegeneration = value; } }

    public int takeDamage(int damage)
    {
        this.currentHealth -= damage;
        if (this.currentHealth <= 0)
        {
            Debug.Log("T'es mort");
        }

        return this.currentHealth;
    }
    
    public void easyMode()
    {
        this.MaxHealth          = (int)(this.MaxHealth * 1.2f);
        this.CurrentHealth      = (int)(this.CurrentHealth * 1.2f);
        this.Force              = (int)(this.Force * 2.0f);
        this.MaxEnergy          = (int)(this.maxEnergy * 1.2f);
        this.CurrentEnergy      = (int)(this.CurrentEnergy * 1.2f);
        this.HpRegeneration     = this.HpRegeneration * 2;
        this.EnergyRegeneration = this.EnergyRegeneration * 2;
    }
    
    public void hardMode()
    {
        this.MaxHealth          = (int)(this.MaxHealth * 0.8f);
        this.CurrentHealth      = (int)(this.CurrentHealth * 0.8f);
        this.Force              = (int)(this.Force * 0.8f);
        this.MaxEnergy          = (int)(this.maxEnergy * 0.8f);
        this.CurrentEnergy      = (int)(this.CurrentEnergy * 0.8f);
        this.HpRegeneration     = this.HpRegeneration / 2;
        this.EnergyRegeneration = this.EnergyRegeneration / 2;
    }


    abstract public void levelUp();

    abstract public int getDamage();
}
