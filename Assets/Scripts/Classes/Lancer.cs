﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lancer : Mobile
{

    // Attributs

    // Constructors
    public Lancer() : base(30, 30, 5, new Closed(300, 1.0f, 1), false, null, 5.0f)
    {
        
    }
    
    public Lancer(int totalHealthValue, int remainingHealthValue, int experienceValue, Weapon weaponRef, bool indestructibleValue, Position positionRef, float movementSpeedValue) :
        base(totalHealthValue, remainingHealthValue, experienceValue, weaponRef, indestructibleValue, positionRef, movementSpeedValue)
    {

    }

    // Properties

}
