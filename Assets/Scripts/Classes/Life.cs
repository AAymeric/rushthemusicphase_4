﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Life : Potion
{

    // Attributs
    private int hpRegeneration;
    
    // Constructors
    public Life() : base()
    {
        hpRegeneration = 1;
    }

    public Life(float timeEffectValue, int hpRegenerationValue) : base(timeEffectValue)
    {
        hpRegeneration = hpRegenerationValue;
    }

    // Properties
    public int HpRegeneration { get { return hpRegeneration; } set { hpRegeneration = value; } }

}
