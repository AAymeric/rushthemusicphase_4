﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magician : Hero
{
    // Constructors
    public Magician() : base(800, 800, 15, 1000, 1000, 10, new Ranged(), "image de base", 0 )
    {
    }

    // Base constructor
    public Magician(int maxHealthValue, int currentHealthValue, float forceValue, int maxEnergyValue, int currentEnergyValue, float hpRegenerationValue, Weapon weaponRef, string imagePath) 
        : base(maxHealthValue, currentHealthValue, forceValue, maxEnergyValue, currentEnergyValue, hpRegenerationValue, weaponRef, imagePath, 0)
    {
        
    }

    // Custom level constructor
    public Magician(int maxHealthValue, int currentHealthValue, float forceValue, int experienceValue, int levelValue, int nextLevelValue, int maxEnergyValue, int currentEnergyValue, float hpRegenerationValue, Weapon weaponRef, string imagePath)
        :base(maxHealthValue, currentHealthValue, forceValue, experienceValue, levelValue, nextLevelValue, maxEnergyValue, currentEnergyValue, hpRegenerationValue, weaponRef, imagePath, 0)
    {

    }

    public override void levelUp()
    {
        switch (this.level)
        {
            case 0 :
                this.level += 1;
                this.nextLevel = 50;
                this.force = this.force * 1.1f;
                break;
            case 1 :
                this.level += 1;
                this.nextLevel = 100;
                this.force = this.force * 1.1f;
                break;
            case 2 : 
                this.level += 1;
                this.nextLevel = 200;
                this.maxHealth = (int)(this.maxHealth * 1.1f);
                this.currentHealth = (int)(this.currentHealth * 1.1f);
                break;
            case 3 :
                this.level += 1;
                this.nextLevel = 200;
                this.force = this.force * 1.1f;
                break;
            case 4 : 
                this.level += 1;
                this.nextLevel = 500;
                break;
            default :
                this.level = 1;
                this.nextLevel = 50;
                break;
        }
    }
    
    public override int getDamage()
    {
        int damage = (int)this.force;
        damage = (int)(this.Force * ((float)this.CurrentEnergy / (float)this.MaxEnergy));
        return damage;
    }

}
