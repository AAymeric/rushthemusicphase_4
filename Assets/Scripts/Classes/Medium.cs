﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Medium : Difficulty
{

    // Constructors
    public Medium() : base(1.0F, "Medium")
    {
        
    }

}
