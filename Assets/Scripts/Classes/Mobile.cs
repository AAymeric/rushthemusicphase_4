﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Mobile : Ennemi
{

    // Attributs

    // Constructors
    public Mobile(int totalHealthValue, int remainingHealthValue, int experienceValue, Weapon weaponRef, bool indestructibleValue, Position positionRef, float movementSpeedValue) : 
        base(totalHealthValue, remainingHealthValue, experienceValue, weaponRef, indestructibleValue, positionRef)
    {
        movementSpeed = movementSpeedValue;
    }

    // Properties
    public float MovementSpeed { get { return movementSpeed; } set { movementSpeed = value; } }

}
