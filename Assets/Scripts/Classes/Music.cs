﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music
{

    // Attributs
    private string source;
    //private enum difficulty { Easy, Medium, Hardcore };
    private float duration;

    // Constructors
    public Music()
    {
        source = "none";
        duration = 2.0F;
    }

    public Music(string sourceValue, float durationValue)
    {
        source = sourceValue;
        duration = durationValue;
    }

    // Properties
    public string Source { get { return source; } set { source = value; } }
    public float Duration { get { return duration; } set { duration = value; } }

}
