﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Position
{

    // Attributs
    private float x;
    private float y;
    private float z;

    // Constructors
    public Position()
    {
        x = 0;
        y = 0;
        z = 0;
    }

    public Position(float xValue, float yValue, float zValue)
    {
        x = xValue;
        y = yValue;
        z = zValue;
    }

    // Properties
    public float X { get { return x; } set { x = value; } }
    public float Y { get { return y; } set { y = value; } }
    public float Z { get { return z; } set { z = value; } }

}
