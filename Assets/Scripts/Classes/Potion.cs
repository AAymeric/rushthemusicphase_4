﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Potion
{

    // Attributs
    private float timeEffect;
    
    // Constructors
    public Potion()
    {
        timeEffect = 1.0F;
    }

    public Potion(float timeEffectValue)
    {
        timeEffect = timeEffectValue;
    }

    // Properties
    public float TimeEffect { get { return timeEffect; } set { timeEffect = value; } }

}
