﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ranged : Weapon
{

    // Attributs
    public float size;
    public float speed;

    // Constructors
    public Ranged() : base()
    {
        size = 1.0F;
        speed = 1.0F;
    }

    public Ranged(int damageValue, float attackSpeedValue, float sizeValue, float speedValue) : base(damageValue, attackSpeedValue)
    {
        size = sizeValue;
        speed = speedValue;
    }
    
    // Properties
    public float Size { get { return size; } set { size = value; } }
    public float Speed { get { return speed; } set { speed = value; } }

}
