﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : Fixed
{

    // Attributs
    private float size;

    // Constructors
    public Wall() : base(30, 30, 5, null, true, null)
    {
        
    }
    
    public Wall(int totalHealthValue, int remainingHealthValue, int experienceValue, Weapon weaponRef, bool indestructibleValue, Position positionRef, float sizeValue)
        : base(totalHealthValue, remainingHealthValue, experienceValue, weaponRef, indestructibleValue, positionRef)
    {
        size = sizeValue;
    }

    // Properties
    public float Size { get { return size; } set { size = value; } }

}
