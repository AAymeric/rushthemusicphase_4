﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warrior : Hero
{
    // Constructors
    public Warrior() : base(1000, 1000, 10.0f, 1000, 1000, 10, new Closed(), "image de base", 50)
    {
    }

    // Base constructor
    public Warrior(int maxHealthValue, int currentHealthValue, float forceValue, int maxEnergyValue, int currentEnergyValue, float hpRegenerationValue, Weapon weaponRef, string imagePath, int energyRegenerationValue)
        : base(maxHealthValue, currentHealthValue, forceValue, maxEnergyValue, currentEnergyValue, hpRegenerationValue, weaponRef, imagePath, energyRegenerationValue)
    {
    }

    // Custom level constructor
    public Warrior(int maxHealthValue, int currentHealthValue, float forceValue, int experienceValue, int levelValue, int nextLevelValue, int maxEnergyValue, int currentEnergyValue, float hpRegenerationValue, Weapon weaponRef, string imagePath, int energyRegenerationValue)
        : base(maxHealthValue, currentHealthValue, forceValue, experienceValue, levelValue, nextLevelValue, maxEnergyValue, currentEnergyValue, hpRegenerationValue, weaponRef, imagePath, energyRegenerationValue)
    {
    }

    public override void levelUp()
    {
        switch (this.level)
        {
            case 0 :
                this.level += 1;
                this.nextLevel = 50;
                this.maxHealth = (int)(this.maxHealth * 1.1f);
                this.currentHealth = (int)(this.currentHealth * 1.1f);
                break;
            case 1 :
                this.level += 1;
                this.nextLevel = 100;
                this.force = this.force * 1.1f;
                break;
            case 2 : 
                this.level += 1;
                this.nextLevel = 200;
                this.hpRegeneration += 5;
                break;
            case 3 :
                this.level += 1;
                this.nextLevel = 200;
                this.hpRegeneration += 5;
                break;
            case 4 : 
                this.level += 1;
                this.nextLevel = 500;
                break;
            default :
                this.level = 1;
                this.nextLevel = 50;
                break;
        }
    }
    
    public override int getDamage()
    {
        int damage = (int)this.force;
        damage = (int)this.Force * (1 + (this.CurrentEnergy / this.MaxEnergy) * 2);
        this.CurrentEnergy = this.CurrentEnergy / 2;
        Debug.Log(CurrentEnergy);
        return damage;
    }
}
