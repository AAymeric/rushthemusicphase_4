﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon
{

    // Attributs
    private int damage;
    private float attackSpeed;

    // Constructors
    public Weapon()
    {
        damage = 10;
        attackSpeed = 1.0F;
    }

    public Weapon(int damageValue, float attackSpeedValue)
    {
        damage = damageValue;
        attackSpeed = attackSpeedValue;
    }

    // Properties
    public int Damage { get { return damage; } set { damage = value; } }
    public float AttackSpeed { get { return attackSpeed; } set { attackSpeed = value; } }

}
