﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarScript : MonoBehaviour {

    //[SerializeField]
    private float fillAmount;

    [SerializeField]
    private float lerpSpeed;

    [SerializeField]
    private Image content;

    [SerializeField]
    private Text valueText;

    [SerializeField]
    private Color FullColor;

    [SerializeField]
    private Color LowColor;

    [SerializeField]
    private bool LerpColors;

    public float MaxValue{ get; set; }

    public float Value
    {
        set
        {
            valueText.text = value + "/" + MaxValue;
            fillAmount = Map(value, 0, MaxValue, 0, 1);
        }
    }

    // Use this for initialization
    void Start ()
    {
		if(LerpColors)
        {
            content.color = FullColor;
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
        HandleBar();
	}

    private void HandleBar()
    {
        if (fillAmount!= content.fillAmount)
        {
            content.fillAmount = Mathf.Lerp(content.fillAmount,fillAmount,Time.deltaTime * lerpSpeed);
        }

        if(LerpColors)
        {
            content.color = Color.Lerp(LowColor, FullColor, fillAmount);
            
        }
        
         
    }

    // Initialize a value 
    private float Map (float value, float MinFin, float MaxFin, float MinInit, float MaxInit)
    {
        //float result;
        return (value - MinFin) * (MaxInit - MinInit) / (MaxFin - MinFin) + MinInit;

    }
}
