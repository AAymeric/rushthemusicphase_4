﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClassImageScript : MonoBehaviour {

    private Image classImage;
    public Sprite warrior;
    public Sprite magician;
    public Sprite monk;
    public GameParameters parameters;

	// Use this for initialization
	void Start () {
        parameters = GameParameters.getInstance();
        classImage = GetComponent<Image>();
        switch (parameters.Hero.GetType().Name)
        {
            case "Monk":
                classImage.sprite = monk;
                break;
            case "Warrior":
                classImage.sprite = warrior;
                break;
            case "Magician":
                classImage.sprite = magician;
                break;
        }
            
        
    }
	
	// Update is called once per frame
	void Update () {
        
	}
}
