﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemiScript : MonoBehaviour {
    
    public Ennemi ennemi { get; set; }
    public TimeKeeper timeKeeper = TimeKeeper.Instance;
    public GameParameters gameParameters = GameParameters.getInstance();
    private bool damageDealt;

    // Use this for initialization
    void Start ()
    {
    }

    void Update()
    {
    }

    public virtual void objectMovement(float timescale, bool isPause)
    {
        if (isPause)
        {
            GetComponent<Rigidbody>().isKinematic = true;
        }
        else
        {
            GetComponent<Rigidbody>().isKinematic = false;
        }
        if (!ennemi.Indestructible)
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0, GetComponent<Rigidbody>().velocity.y, -ennemi.movementSpeed * timescale);
        }
        else
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0, GetComponent<Rigidbody>().velocity.y, 10);
        }
    }

    public virtual void startRun()
    {
        GetComponent<Rigidbody>().isKinematic = false;
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, -ennemi.movementSpeed);
    }

    private void stopRun()
    {
        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
    }

    public virtual void takeDamage(int damage)
    {
        if (!ennemi.Indestructible && ennemi.RemainingHealth>0)
        {
            Debug.Log("Outch, I took " + damage);
            if (gameParameters.Hero is Monk || gameParameters.Hero is Warrior)
            {
                GameObject.FindWithTag("SFX").GetComponent<FxManager>().makeDamage.Play();                
            }
            ennemi.RemainingHealth -= damage;
            if (ennemi.RemainingHealth <= 0)
            {
                destroy();
            }
            ennemi.Indestructible = true;
            GetComponent<Rigidbody>().velocity = new Vector3(0, GetComponent<Rigidbody>().velocity.y, 10);
            Invoke("stopInvulnerable", 0.5f);
        }
    }


    public virtual void OnTriggerEnter(Collider col)
    {
        if (!(ennemi is DragonWhelp))
        {
            if (col.gameObject.CompareTag("Player"))
            {
                if (!damageDealt)
                {
                    damageDealt = true;
                    Debug.Log("Degats !");
                    if (this.ennemi.RemainingHealth > 0)
                    {
                        if (!(ennemi is Fixed))
                        {
                            col.gameObject.GetComponent<Player>().takeDamage(ennemi.Weapon.Damage);
                        }
                        UnityEngine.Object.Destroy(this.gameObject);
                    }
                }
            }
            
            if (col.gameObject.CompareTag("Shield"))
            {
                if (this.ennemi.RemainingHealth > 0)
                {
                    Debug.Log("Parade !");
                    Debug.Log(gameParameters.Hero);
                    if (gameParameters.Hero is Monk && !(ennemi is Chest))
                    {
                        int damage = ennemi.Weapon.Damage / 2;
                        col.gameObject.GetComponentInParent<Player>().takeDamage(damage);
                        Debug.Log("damage : " + damage);
                    }
                }
                UnityEngine.Object.Destroy(this.gameObject);
            }

        }

        if (col.gameObject.CompareTag("Weapon"))
        {
            Debug.Log("Attaque !");
			this.takeDamage(gameParameters.Hero.getDamage());
        }
    }

    public virtual void destroy()
    {
        gameObject.layer = LayerMask.NameToLayer("DeadEnnemiLayer");
        UnityEngine.Object.Destroy(this.gameObject,1);
        Rigidbody r = GetComponent<Rigidbody>();
        if (r)
        {
            r.constraints = 0;
        }
        GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().gainExperince(ennemi.Experience);
    }


    private void stopInvulnerable()
    {
        ennemi.Indestructible = false;
    }
}
