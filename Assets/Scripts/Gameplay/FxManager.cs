﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FxManager : MonoBehaviour
{

	public AudioSource potionDrink;
	public AudioSource potionPickUp;
	public AudioSource takeDamage;
	public AudioSource makeDamage;
	public AudioSource healing;
    
	public Animator healAnimator;
	public Animator energyAnimator;
	public Animator powerAnimator;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void pickUp(int potion)
	{
		potionPickUp.Play();
		switch (potion)
		{
				case 0 :
					healAnimator.SetTrigger("Up");
					break;
				case 1 :
					energyAnimator.SetTrigger("Up");
					break;
				case 2 :
					powerAnimator.SetTrigger("Up");
					break;
		}
	}
}
