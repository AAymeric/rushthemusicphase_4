﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HastScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void OnTriggerEnter(Collider col)
	{
		if (col.CompareTag("Shot"))
		{
			Destroy(col.gameObject);
			GameObject player   = GameObject.FindGameObjectWithTag("Player");
			Player playerScript = player.GetComponent<Player>();
			playerScript.takeDamage(10);
		}
	}
}
