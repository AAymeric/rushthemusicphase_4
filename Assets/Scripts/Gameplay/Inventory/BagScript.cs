﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BagScript : MonoBehaviour {

    [SerializeField]
    private GameObject slotPrefab;

    private List<SlotScript> slots = new List<SlotScript>();

    public List<SlotScript> MySlots
    {
        get
        {
            return slots;
        }
        set
        {
            slots = value;
        }

    }

  

    public void AddSlots(int slotsCounter)
    {
        for (int i = 0; i < slotsCounter; i++)
        {
           SlotScript slot = Instantiate(slotPrefab, transform).GetComponent<SlotScript>();
           MySlots.Add(slot);
        }
    }

    public bool AddItem(Item item)
    {
        foreach (SlotScript slot in MySlots)
        {
            if (slot.IsEmpty)
            {
                slot.AddItem(item);
                return true;
            }
           
        }
        return false;
    }
}
