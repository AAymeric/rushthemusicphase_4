﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
	[SerializeField] 
	public Text stackSizeVie;

    [SerializeField]
    public Text stackSizeMana;

    [SerializeField]
    public Text stackSizeExperience;

    public Player play;
    
    public Animator healAnimator;
    public Animator energyAnimator;
    public Animator powerAnimator;

    void Start()
	{
	}

	void Update()
	{

		stackSizeVie.text = GameParameters.getInstance().Hero.Inventory[0].ToString();
        stackSizeMana.text = GameParameters.getInstance().Hero.Inventory[1].ToString();
        stackSizeExperience.text = GameParameters.getInstance().Hero.Inventory[2].ToString();
        

        
    }
    public void useEnergyPotion()
    {
        if (GameParameters.getInstance().Hero.Inventory[1] > 0)
        {
            energyAnimator.SetTrigger("Down");
            GameParameters.getInstance().Hero.Inventory[1]--;
        }
    }

    public void useExperiencePotion()
    {
       if (GameParameters.getInstance().Hero.Inventory[2] > 0)
        {
            powerAnimator.SetTrigger("Down");
            GameParameters.getInstance().Hero.Inventory[2]--;
        }
    }
}