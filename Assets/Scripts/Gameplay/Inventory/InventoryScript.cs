﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventoryScript : MonoBehaviour
{
    // Pour le debug
    [SerializeField]
    private Item[] items;

    public static InventoryScript instance;

    private List<Bag> bags = new List<Bag>();

    private Potions healthpotion;

    private Potions manapotion;

    private Potions furypotion;

    public Player play;

    public static InventoryScript MyInstance
    {
        get
        {
            if(instance==null)
            {
                instance = FindObjectOfType<InventoryScript>();
            }
            return instance;
        }
    }

    public void AddItem(int idPotion)
    {
        Item item;
        switch (idPotion)
        {
            case 0:
                item = healthpotion;
                break;
            case 1:
                item = manapotion;
                break;
            case 2:
                item = furypotion;
                break;
            default:
                item = healthpotion;
                break;
        }
        if (item.MaxStackSize>0)
        {
            item.stackSize++;

            if (PlaceInStack(item))
            {
                
                return;
            }
        }
        PlaceInEmpty(item);
        
    }

    

    private void PlaceInEmpty(Item item)
    {
        foreach(Bag bag in bags)
        {
            if(bag.MyBagScript.AddItem(item))
            {
                return;
            }
        }
    }
    private bool PlaceInStack(Item item)
    {
        foreach (Bag bag in bags)
        {
            foreach(SlotScript slots in bag.MyBagScript.MySlots)
            {
                if (slots.StackItem(item))
                {
                    UpdateEachStackSize();
                    return true;
                }
                
            }
        }
        return false;
    }

    public void Awake()
    {
        
        Bag bag = (Bag)Instantiate(Items[0]);
        bag.Initialize(3);
        bag.Use();
        furypotion = (Potions)Instantiate(Items[3]);
        manapotion = (Potions)Instantiate(Items[2]);
        healthpotion = (Potions)Instantiate(Items[1]);
        AddItem(0);
        AddItem(1);
        AddItem(2);
        furypotion.stackSize = 0;
        manapotion.stackSize = 0;
        healthpotion.stackSize = 0;
    }


    void Update()
    {
        
        /*if (Input.GetKeyDown(KeyCode.P))
        {
            AddItem(0);
        }
*/
         
    }
    [SerializeField]
    private BagButton[] bagButtons;

    public bool CanAddBag
    {
        get { return bags.Count < 10; }
    }

    public Item[] Items
    {
        get
        {
            return items;
        }

        set
        {
            items = value;
        }
    }

    

    // Use this for initialization
    void Start () {
		
	}
	
    public void UpdateStackSize (SlotScript clickable)
    {

        clickable.MyStackText.text = clickable.MyItem.MyStackSize.ToString();
        clickable.MyStackText.color = Color.black;
        clickable.MyIcon.color = Color.white;
    }

    public void UpdateEachStackSize()
    {
        foreach (Bag bag in bags)
        {
            foreach (SlotScript slots in bag.MyBagScript.MySlots)
            {
                UpdateStackSize(slots);
            }
        }
    }

    public void AddBag(Bag bag)
    {
        foreach (BagButton bagButton in bagButtons)
        {
            if (bagButton.MyBag == null)
            {
                bagButton.MyBag = bag;
                bags.Add(bag);
                break;
            }
        }
    }


    public void useHealthPotion()
    {
        if (healthpotion.MyStackSize > 0 && play.health.CurrentVal < play.health.MaxVal)
        {
            healthpotion.Remove();
            play.useLifePotion();
            UpdateEachStackSize();
        }
    }

    public void useEnergyPotion()
    {
        if (manapotion.MyStackSize > 0)
        {
            manapotion.Remove();
            play.useEnergyPotion();
            UpdateEachStackSize();
        }
    }

    public void useInvulnerablePotion()
    {
        if (furypotion.MyStackSize > 0)
        {
            furypotion.Remove();
            play.useInvulnerablePotion();
            UpdateEachStackSize();
        }
    }

    /*
    public void InteractPotions()
    {
        if(health.CurrentVal <=95)
        {
            potion.Remove();
            play.health.CurrentVal += 5;
        }
        
    }
    */
}
