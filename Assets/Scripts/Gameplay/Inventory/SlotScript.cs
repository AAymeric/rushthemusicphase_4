﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SlotScript : MonoBehaviour, IPointerClickHandler, IClickable {

    private ObservableStack<Item> items = new ObservableStack<Item>();

    [SerializeField]
    private Image icon;

    [SerializeField]
    public Text stackSize;
    public bool IsEmpty
    {
        get
        {
            return Items.Count == 0;
            //return Items.Count <= 1;
        }
    }

  
    public Item MyItem
    {
        get
        {
            if (!IsEmpty)
            {
                return Items.Peek();
            }
            return null;
        }
    }

    public Image MyIcon
    {
        get
        {
            return icon;
        }

        set
        {
            icon = value;
        }
    }

    public int MyCount
    {
        get
        {
            return Items.Count;
        }
    }

    public Stack<Item> Items
    {
        get
        {
            return items;
        }    
    }

    public Text MyStackText
    {
        get
        {
           return stackSize;
        }
    }

    public void Awake()
    {
        items.OnPop += new UpdateStackEvent(UpdateSlot);
        items.OnPush += new UpdateStackEvent(UpdateSlot);
        items.OnClear += new UpdateStackEvent(UpdateSlot);
    }
    public bool AddItem(Item item)
    {
        Items.Push(item);
        icon.sprite = item.MyIcon;
        icon.color = Color.white;
        item.MySlot = this;
        return true;
    }

    public void RemoveItem(Item item)
    {
        if(!IsEmpty && item.name == MyItem.name && item.stackSize > 0)
        {
            Items.Pop();
            InventoryScript.MyInstance.UpdateStackSize(this);
        }
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            UseItem();
        }
    }

    public void UseItem()
    {
        if (MyItem is IUseable)
        {
            (MyItem as IUseable).Use();
        }
    }

    public bool StackItem(Item item)
    {
        if(!IsEmpty && item.name == MyItem.name && item.stackSize < MyItem.MaxStackSize)
        {
            items.Push(item);
            item.MySlot = this;
            return true;
        }
        return false;
    }

    private void UpdateSlot()
    {
        InventoryScript.MyInstance.UpdateStackSize(this);
    }
}
