﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item : ScriptableObject
{
    [SerializeField]
    private Sprite icon;

    [SerializeField]
    public int stackSize;


    [SerializeField]
    private int maxStackSize;

    private SlotScript Slot;

    public Sprite MyIcon
    {
        get
        {
            return icon;
        }
    }

    public int MyStackSize
    {
        get
        {
            return stackSize;
        }

    }

    public int MaxStackSize
    {
        get
        {
            return maxStackSize;
        }
    }

    public SlotScript MySlot
    {
        get
        {
            return this.Slot;
        }

        set
        {
            this.Slot = value;
        }
    }

    public void Remove()
    {
        if (MyStackSize > 0)
        {
            stackSize--;
            if (MySlot != null)
            {
                MySlot.RemoveItem(this);
            }
        }
    }
}
