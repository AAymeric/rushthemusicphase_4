﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Potion", menuName = "Items/Potion", order = 1)]
public class Potions : Item, IUseable
{
    [SerializeField]
    private int Myhealth;

    public void Use()
    {
        Remove();
    }
    
}
