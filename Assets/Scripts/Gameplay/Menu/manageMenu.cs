﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class manageMenu : MonoBehaviour
{
	public InputField input;
	private GameParameters gameParameters = GameParameters.getInstance();
	
	private enum Hand { Right, Left }

	// Use this for initialization
	void Start ()
	{
		if (this.name.Equals("ContinuButton"))
		{
			if (PlayerPrefs.GetInt("levelNumber", 0) != 0)
			{
				this.GetComponent<Button>().enabled = true;
				this.GetComponent<Animator>().enabled = true;
				this.transform.GetChild(0).GetComponent<Text>().color = Color.white;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void chooseWarrior()
	{
		gameParameters.Hero = new Warrior();
	}
	
	public void chooseMonk()
	{
		gameParameters.Hero = new Monk();
	}
	
	public void chooseMagician()
	{
		gameParameters.Hero = new Magician();
	}
	
	public void chooseRight()
	{
		gameParameters.MainHand = (int)Hand.Right;
	}

	public void chooseLeft()
	{
		gameParameters.MainHand = (int)Hand.Left;
	}
	
	public void chooseName()
	{
		gameParameters.Name = input.text;
	}

	public void chooseDifficulty(int difficulty)
	{
		if (difficulty == 0)
		{
			gameParameters.Difficulty = new Easy();
		}
		else if (difficulty == 1)
		{
			gameParameters.Difficulty = new Medium();
		}
		else if (difficulty == 2)
		{
			gameParameters.Difficulty = new Hard();
		}
	}
	
	public void continuButton()
	{
		gameParameters.restaureSave();
        int scenetoLoad = gameParameters.LevelNumber;
        if(scenetoLoad> SceneManager.sceneCountInBuildSettings)
        {
            scenetoLoad -= SceneManager.sceneCountInBuildSettings - 1;
            scenetoLoad = scenetoLoad % 3;
            scenetoLoad += 4;
        }

        this.GetComponent<LoadSceneOnClick>().LoadByIndex(scenetoLoad);
	}

	public void startNewGame()
	{
		gameParameters.LevelNumber = 1;
		gameParameters.save();
	}
}
