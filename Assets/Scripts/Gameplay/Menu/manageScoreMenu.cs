﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class manageScoreMenu : MonoBehaviour {

	// Use this for initialization
	void Start ()
	{
		String name;
		int score;
		int children = transform.childCount;
		for (int i = 1; i < children; ++i)
		{
			if (PlayerPrefs.HasKey("name" + (i)) && PlayerPrefs.HasKey("score" + (i)))
			{
				name = PlayerPrefs.GetString("name" + (i));
				score = PlayerPrefs.GetInt("score" + (i));
			}
			else
			{
				name  = "";
				score = 0;
			}
			transform.GetChild(i).GetChild(0).gameObject.GetComponent<UnityEngine.UI.Text>().text = name;
			transform.GetChild(i).GetChild(1).gameObject.GetComponent<UnityEngine.UI.Text>().text = score.ToString();
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
