﻿using System;
using System.Collections;
using System.Collections.Generic;
using LeapInternal;
//using UnityEditor.PackageManager;
using UnityEngine;

public class Player : MonoBehaviour {
    
    private GameParameters gameParametersInstance;
    private Hero heroModel;
    private bool isDead;
    private TimeKeeper timeKeeper = TimeKeeper.Instance;
    private bool powerMod = false;
    private bool isInvunerable = false;
    private bool isHealing = false;
    private bool monkPower = false;
    private enum Hand { Right, Left }
    private float hot = 0.0f;
    private GameObject manager;
    private GameObject fxManager;
    public bool prun;
    public Animator damageTakenAnimator;
    public Animator healingAnimator;
    private int timeBeforeAnimate;


    [SerializeField]
    public Stat xp;
    
    [SerializeField]
    public Stat mana;

    [SerializeField]
    public Stat health;
    private bool animate;
    private int frameAnimate;

    void Start()
    {
        this.gameParametersInstance = GameParameters.getInstance();
        this.heroModel = this.gameParametersInstance.Hero;
        this.heroModel.CurrentHealth = this.heroModel.MaxHealth;
        timeBeforeAnimate = 0;
        //this.heroModel = new Magician();
        //GameParameters.getInstance().Hero = this.heroModel; // for test
        this.isDead = false;
        this.prun = true;
        manager = GameObject.FindWithTag("AudioManager");
        fxManager = GameObject.FindWithTag("SFX");
        
        xp.Initialize(heroModel.NextLevel, 0);
        mana.Initialize(heroModel.MaxEnergy, heroModel.MaxEnergy);
        health.Initialize(heroModel.CurrentHealth, heroModel.MaxHealth);

        


        if (this.heroModel is Warrior)
        {
            GameObject.FindGameObjectWithTag("MonkSetR").SetActive(false);
            GameObject.FindGameObjectWithTag("MonkSetL").SetActive(false);
            GameObject.FindGameObjectWithTag("MonkSet").SetActive(false);
            GameObject.FindGameObjectWithTag("MagicianSetR").SetActive(false);
            GameObject.FindGameObjectWithTag("MagicianSetL").SetActive(false);
            GameObject.FindGameObjectWithTag("MagicianSet").SetActive(false);
            GameObject.FindGameObjectWithTag("FireballManager").SetActive(false);
            if (gameParametersInstance.MainHand == (int)Hand.Right)
            {
                GameObject.FindGameObjectWithTag("WarriorSetR").SetActive(true);
                GameObject.FindGameObjectWithTag("WarriorSetL").SetActive(false);
            }
            else
            {
                GameObject.FindGameObjectWithTag("WarriorSetL").SetActive(true);
                GameObject.FindGameObjectWithTag("WarriorSetR").SetActive(false);
            }
        }
        else if (this.heroModel is Monk)
        {
            GameObject.FindGameObjectWithTag("WarriorSetR").SetActive(false);
            GameObject.FindGameObjectWithTag("WarriorSetL").SetActive(false);
            GameObject.FindGameObjectWithTag("MagicianSetR").SetActive(false);
            GameObject.FindGameObjectWithTag("MagicianSetL").SetActive(false);
            GameObject.FindGameObjectWithTag("MagicianSet").SetActive(false);
            GameObject.FindGameObjectWithTag("MonkSet").SetActive(true);
            GameObject.FindGameObjectWithTag("FireballManager").SetActive(false);
            if (gameParametersInstance.MainHand == (int)Hand.Right)
            {
                GameObject.FindGameObjectWithTag("MonkSetR").SetActive(true);
                GameObject.FindGameObjectWithTag("MonkSetL").SetActive(false);
            }
            else
            {
                GameObject.FindGameObjectWithTag("MonkSetL").SetActive(true);
                GameObject.FindGameObjectWithTag("MonkSetR").SetActive(false);
            }
        }
        else if (this.heroModel is Magician)
        {
            GameObject.FindGameObjectWithTag("WarriorSetR").SetActive(false);
            GameObject.FindGameObjectWithTag("WarriorSetL").SetActive(false);
            GameObject.FindGameObjectWithTag("MonkSetR").SetActive(false);
            GameObject.FindGameObjectWithTag("MonkSetL").SetActive(false);
            GameObject.FindGameObjectWithTag("MonkSet").SetActive(false);
            GameObject.FindGameObjectWithTag("MagicianSet").SetActive(true);
            GameObject.FindGameObjectWithTag("FireballManager").SetActive(true);
            if (gameParametersInstance.MainHand == (int)Hand.Right)
            {
                GameObject.FindGameObjectWithTag("MagicianSetR").SetActive(true);
                GameObject.FindGameObjectWithTag("MagicianSetL").SetActive(false);
            }
            else
            {
                GameObject.FindGameObjectWithTag("MagicianSetL").SetActive(true);
                GameObject.FindGameObjectWithTag("MagicianSetR").SetActive(false);
            }
        }
        InvokeRepeating("regenerationLife", 1.0f, 1.0f);
        if (this.heroModel.EnergyRegeneration > 0)
        {
            InvokeRepeating("regenerationEnergy", 1.0f, 1.0f);
        }
        
        Invoke("setPrunFalse", 3.0f);
        animate = false;
        timeKeeper.unPause();
    }

    // Update is called once per frame
    void Update()
    {
        //startRun();
        //GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 5);
        if (timeBeforeAnimate > 0)
        {
            timeBeforeAnimate--;
        }
        if (!prun)
        {
            objectMovement(timeKeeper.timescale, timeKeeper.isPause());
        }
        if (this.health.CurrentVal <= 0)
        {
            death();
        }
        if (Input.GetKeyDown(KeyCode.X) && !animate)
        {
            if (GameObject.FindGameObjectWithTag("DirectingHand"))
            {
                animate = true;
                frameAnimate = 0;
            }
        }
        if (animate)
        {
            if(frameAnimate == 0)
            {
                GameObject.FindGameObjectWithTag("DirectingHand").transform.Rotate(0, 0, -120);
                GameObject.FindGameObjectWithTag("DirectingHand").transform.Rotate(0, -40, 0);
            }
            if (frameAnimate < 15)
            {
                GameObject.FindGameObjectWithTag("DirectingHand").transform.Rotate(0, 10, 0);
                frameAnimate++;
            }
            else
            {
                GameObject.FindGameObjectWithTag("DirectingHand").transform.Rotate(0, -110, 120);
                animate = false;
            }
        }
        if (Input.GetKeyDown("[1]"))
        {
            useLifePotion();
        }
        if (Input.GetKeyDown("[2]"))
        {
            useEnergyPotion();
        }
        if (Input.GetKeyDown("[3]"))
        {
            useInvulnerablePotion();
        }
        if(heroModel is Monk)
        {
            if (Input.GetKeyDown(KeyCode.M))
            {
                startMonkPower();
            }
            if (Input.GetKeyDown(KeyCode.L))
            {
                stopMonkPower();
            }
        }
        if (this.heroModel.Level == 5)
        {
            if (this.heroModel is Warrior)
            {
                if (!isInvunerable)
                {
                    Invoke("becomeInvulnerable", 30);
                }
            }
            else if (this.heroModel is Monk)
            {
                if (!isHealing)
                {
                    this.isHealing = true;
                    this.gainLife(15);
                    Invoke("stopHealing", 15);
                }
            }
        }
        
        if (monkPower && this.heroModel.CurrentEnergy > 0 && this.heroModel.CurrentHealth < this.heroModel.MaxHealth)
        {
            gainLife((int)(Math.Floor(this.hot)));
            this.heroModel.CurrentEnergy -= (int)(Math.Floor(this.hot));
            this.hot += 0.02f;
        }
        else
        {
            stopMonkPower();
        }

        xp.MaxVal         = this.heroModel.NextLevel;
        xp.CurrentVal     = this.heroModel.Experience;
        health.MaxVal     = this.heroModel.MaxHealth;
        health.CurrentVal = this.heroModel.CurrentHealth;
        mana.MaxVal       = this.heroModel.MaxEnergy;
        mana.CurrentVal   = this.heroModel.CurrentEnergy;
    }

    private void setPrunFalse()
    {
        this.prun = false;
    }
    
    private void objectMovement(float timescale, bool isPause)
    {
        if (isPause)
        {
            GetComponent<Rigidbody>().isKinematic = true;
        }
        else
        {
            GetComponent<Rigidbody>().isKinematic = false;
        }
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 5 * timescale);

    }

    private void startRun()
    {
        objectMovement(timeKeeper.timescale, timeKeeper.isPause());
        //GetComponent<Rigidbody>().isKinematic = false;
        //GetComponent<Rigidbody>().velocity = new Vector3(0, 0, -5);
    }
    
    private void stopRun()
    {
        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
    }

    public void takeDamage(int damage)
    {
        if (!this.isInvunerable)
        {
            health.CurrentVal = this.heroModel.takeDamage(damage);
            if (timeBeforeAnimate <= 0)
            {
                fxManager.GetComponent<FxManager>().takeDamage.Play();
                damageTakenAnimator.SetTrigger("Breath");
                timeBeforeAnimate = 90;
            }
            
            if (this.health.CurrentVal <= 0)
            {
                death();
            } 
            becomeInvulnerable(0.5f);
        }
    }

    public void gainExperince(int xp)
    {
        //Si le montant total dépasse l'xp total max
        this.heroModel.Experience += xp;
        this.gameParametersInstance.Score += xp;
        while (this.heroModel.Experience > this.heroModel.NextLevel && this.heroModel.Level < 5)
        {
            this.heroModel.levelUp();
        }
    }

    public void gainLife(int life)
    {
        if (this.heroModel.CurrentHealth < this.heroModel.MaxHealth)
        {
            //Si le montant total dépasse la vie total max
            if (this.heroModel.CurrentHealth + life > this.heroModel.MaxHealth)
            {
                life = this.heroModel.MaxHealth - this.heroModel.CurrentHealth;
            }
            this.heroModel.CurrentHealth += life;
        }
    }
    
    public void gainEnergy(int energy)
    {
        if (this.heroModel.CurrentEnergy < this.heroModel.MaxEnergy)
        {
            //Si le montant total dépasse l'énergie total max
            if (this.heroModel.CurrentEnergy + energy > this.heroModel.MaxEnergy)
            {
                energy = this.heroModel.MaxEnergy - this.heroModel.CurrentEnergy;
            }
            this.heroModel.CurrentEnergy += energy;
        }
    }

    public void useEnergyPotion()
    {
        if (heroModel.Inventory[1] > 0)
        {
            fxManager.GetComponent<FxManager>().potionDrink.Play();
            fxManager.GetComponent<FxManager>().energyAnimator.SetTrigger("Down");
            heroModel.Inventory[1]--;
            this.gainEnergy(this.heroModel.MaxEnergy);
            this.powerMod = true;
            Invoke("stopPowerMode", 10);
        }
    }

    public void stopPowerMode()
    {
        this.powerMod = false;
    }
    
    public void useLifePotion()
    {
        Debug.Log(heroModel);
        if (heroModel.Inventory[0] > 0 && health.CurrentVal < health.MaxVal)
        {
            fxManager.GetComponent<FxManager>().potionDrink.Play();
            fxManager.GetComponent<FxManager>().healAnimator.SetTrigger("Down");
            heroModel.Inventory[0]--;
            this.gainLife(this.heroModel.MaxHealth);
        }
    }
    
    public void useInvulnerablePotion()
    {
        if (heroModel.Inventory[2] > 0)
        {
            fxManager.GetComponent<FxManager>().potionDrink.Play();
            fxManager.GetComponent<FxManager>().powerAnimator.SetTrigger("Down");
            heroModel.Inventory[2]--;
            this.isInvunerable = true;
            Invoke("stopInvulnerable", 10);
        }
    }
    
    public void stopInvulnerable()
    {
        this.isInvunerable = false;
    }
    
    public void stopHealing()
    {
        this.isHealing = false;
    }
    
    public void takeExperience(int xp)
    {
        this.heroModel.Experience += xp;
    }

    public void startMonkPower()
    {
        this.healingAnimator.SetTrigger("Breath");
        fxManager.GetComponent<FxManager>().healing.Play();
        this.hot = 0.0f;
        this.monkPower = true;
        Debug.Log("startMonkPower");
    }
    
    public void stopMonkPower()
    {
        this.healingAnimator.SetTrigger("StopBreath");
        if (fxManager)
        {
            fxManager.GetComponent<FxManager>().healing.Stop();
        }
        this.hot = 0.0f;
        this.monkPower = false;
    }

    private void becomeInvulnerable(float time = 5.0f)
    {
        Invoke("stopInvulnerable", time);
    }

    private void death()
    {
        if (!this.isDead)
        {
            this.isDead = true;
            manager.GetComponent<manageTime>().showDefeatMenu(); 
        }
    }

    private void regenerationLife()
    {
        this.gainLife((int)(this.heroModel.HpRegeneration * timeKeeper.timescale));
    }
    
    private void regenerationEnergy()
    {
        this.gainEnergy((int)(this.heroModel.EnergyRegeneration * timeKeeper.timescale));
    }
}
