﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalAnim : MonoBehaviour {

	public Texture[] frame;
	private int framesPerSecond = 30;
	private TimeKeeper timekeeper;
	
	// Use this for initialization
	void Start () {
		timekeeper = TimeKeeper.Instance;
	}
	
	// Update is called once per frame
	void Update () {
		if (!timekeeper.isPause())
		{
			int index = (int)(Time.time * framesPerSecond) % frame.Length;
			GetComponent<Renderer>().material.mainTexture = frame[index];
		}
	}
}
