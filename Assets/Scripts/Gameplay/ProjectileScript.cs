﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileScript : MonoBehaviour
{
	public Ennemi ennemi;
	private GameParameters gameParameters = GameParameters.getInstance();
    private TimeKeeper time = TimeKeeper.Instance;
    private Vector3 pausedVelocity;
    private bool hit;
    private float timeAlive;
	// Use this for initialization
	void Start ()
    {
        timeAlive = 0;
        hit = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (time.isPause())
        {
            if (!GetComponent<Rigidbody>().isKinematic)
            {
                Vector3 vel = GetComponent<Rigidbody>().velocity;
                pausedVelocity = new Vector3(vel.x, vel.y, vel.z);
                GetComponent<Rigidbody>().isKinematic = true;
            }
        }
        else if (pausedVelocity != null && GetComponent<Rigidbody>().isKinematic)
        {
            GetComponent<Rigidbody>().isKinematic = false;
            GetComponent<Rigidbody>().velocity = pausedVelocity;
        }
        if (!time.isPause())
        {
            timeAlive += Time.deltaTime;
            if (timeAlive > 4)
            {
                Destroy(this);
            }
        }
	}
	
	public virtual void OnTriggerEnter(Collider col)
	{
		if (!hit && col.gameObject.CompareTag("Player"))
		{
            hit = true;
			Debug.Log("Degats boulet !");
            if (ennemi is Canon)
            {
                col.gameObject.GetComponent<Player>().takeDamage(ennemi.Weapon.Damage);
            }
            else
            {
                Debug.Log("C'est niké");
            }
			UnityEngine.Object.Destroy(this.gameObject);
		}

		if (!hit && col.gameObject.CompareTag("Shield"))
		{
            hit = true;
			Debug.Log("Parade boulet!");
			if (gameParameters.Hero is Monk)
            {
                if (ennemi is Canon)
                {
                    int damage = ennemi.Weapon.Damage/2;
				    col.gameObject.GetComponentInParent<Player>().takeDamage(damage);
                }

            }
			UnityEngine.Object.Destroy(this.gameObject);
		}
	}
}
