﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    public Text scoreText;
    public Text highScoreText;
    public Text niveau;
    public int scoreCount;
    public int highScoreCount;
    
    
    private GameParameters gameParameters = GameParameters.getInstance();

	void Start ()
    {
        PlayerPrefs.SetInt("HighScore", PlayerPrefs.GetInt("score1", 0));
        highScoreCount = PlayerPrefs.GetInt("HighScore");
    }
	
	void Update ()
    {
        scoreCount = gameParameters.Score;
        
        if (scoreCount > highScoreCount)
        {
            highScoreCount = scoreCount;
            PlayerPrefs.SetInt("HighScore", highScoreCount);
        }
        scoreText.text = "Score: " + Mathf.Round(scoreCount);
        highScoreText.text = "High Score: " + Mathf.Round(highScoreCount);
        niveau.text = "Niv." + gameParameters.LevelNumber;
        
    }
}
