﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraScriptFocusPrincess : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Invoke("begin", 2.0f);
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.y > 28)
		{
			GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
		}
	}

	void begin()
	{
		GetComponent<Rigidbody>().velocity = new Vector3(-1.2f, 3, 2.5f);
	}
}
