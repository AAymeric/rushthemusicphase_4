﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraScriptLancer : MonoBehaviour
{
	private bool ready = false;
	// Use this for initialization
	void Start () {
		Invoke("begin", 2.0f);
	}
	

	// Update is called once per frame
	void Update ()
	{
		if (ready)
			transform.Rotate(Vector3.right * Time.deltaTime * 4);
	}

	void begin()
	{
		GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 2);
		ready = true;
	}

}
