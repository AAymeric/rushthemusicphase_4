﻿using UnityEngine;
using System.Collections;
 
//=========================================================================
// Class: TimeKeeper
// Mirrors Unity's Time.time, allowing the game to be logically paused
// while the animations, UI, and other Time.time dependant tasks keep running at the usual pace.
// TimeKeeper can be paused by the game when timestamp is set to 0,
// or by the user pressing the Pause/Break key.
//=========================================================================
public class TimeKeeper{
 
	//========================================================================= 
	private static readonly TimeKeeper instance = new TimeKeeper();
	
	private bool  mPaused = false;
 
	// Variable: timescale
	// Current timescale of the TimeKeeper.
	public float timescale
	{
		get { return instance.mPaused ? 0 : 1; }
	}

	public bool isPause()
	{
		return instance.mPaused;
	}

	public void pause()
	{
		instance.mPaused = !instance.mPaused;
	}

	public void forcePause()
	{
		instance.mPaused = true;
	}

	public void unPause()
	{
		instance.mPaused = false;
	}
	
	public static TimeKeeper Instance
	{
		get
		{
			return instance;
		}
	}
}

