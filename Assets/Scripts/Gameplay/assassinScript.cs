﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class assassinScript : EnnemiScript {

    private float assassinJump = 0;

	// Use this for initialization
	void Start ()
    {
        ennemi = new Assassin();
    }
	
	// Update is called once per frame
	void Update ()
    {
        objectMovement(timeKeeper.timescale, timeKeeper.isPause());
        //transform.position = new Vector3((Mathf.Clamp(transform.position.x, -1, 1)), transform.position.y, transform.position.z);
        assassinJump+= Time.deltaTime;
        if(assassinJump > 3)
        {
            GetComponent<Rigidbody>().AddForce(0, 5f, 0, ForceMode.VelocityChange);
            assassinJump = 0;
        }
    }

    public override void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            Debug.Log("Degats !");
            if (this.ennemi.RemainingHealth > 0)
            {
                if(!(ennemi is Fixed))
                {
                    col.gameObject.GetComponent<Player>().takeDamage(ennemi.Weapon.Damage);
                }
                UnityEngine.Object.Destroy(this.gameObject);
            }
        }

        if (col.gameObject.CompareTag("Weapon"))
        {
            Debug.Log("Attaque !");
            this.takeDamage(gameParameters.Hero.getDamage());
        }

    }
}
