﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class canonScript : EnnemiScript {

    [SerializeField]
    public GameObject cannonballInstance;

    private float cannonTimer = 0;

    // Use this for initialization
    void Start () {
        ennemi = new Canon();
        Debug.Log(ennemi is Canon);
    }

    // Update is called once per frame
    void Update () {
        if (!timeKeeper.isPause())
        {
            if (transform.position.z - 10 > GameObject.FindGameObjectWithTag("Player").transform.position.z)
            {
                cannonTimer += Time.deltaTime*Random.Range(0, 3);
                if (cannonTimer > 4)
                {
                    float time = 2f;
                    float playerVelocity = GameObject.FindGameObjectWithTag("Player").transform.GetComponent<Rigidbody>().velocity.z;
                    Debug.Log(GameObject.FindGameObjectWithTag("Player").transform.position + " " + playerVelocity);
                    FireCannonAtPoint(GameObject.FindGameObjectWithTag("Player").transform.position + new Vector3(0, 0, playerVelocity * time), time);
                    cannonTimer = 0;
                }
            }
            if (transform.position.z +50 < GameObject.FindGameObjectWithTag("Player").transform.position.z)
            {
                destroy();
            }
        }

    }

    private void FireCannonAtPoint(Vector3 point, float time)
    {
        var velocity = BallisticVelocity(transform.position + new Vector3(0,1,0), point,time);
        Debug.Log("Firing at " + point + " velocity " + velocity);
        GameObject go = Instantiate(cannonballInstance);
        go.GetComponent<ProjectileScript>().ennemi = ennemi;
        go.transform.position = transform.position + new Vector3(0, 1, 0);
        go.GetComponent<Rigidbody>().velocity = velocity;
    }

    private Vector3 BallisticVelocity(Vector3 origin, Vector3 destination, float time)
    {
        float vx = (destination.x - transform.position.x) / time;
        float vz = (destination.z - transform.position.z) / time;
        float vy = (0 - 0.5f * Physics.gravity.y * time * time) / time;

        return new Vector3(vx, vy, vz);
    }
}
