﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class chestScript : EnnemiScript
{

	private FxManager fxManager;
	
	// Use this for initialization
	void Start () {
        ennemi = new Chest();
		gameParameters = GameParameters.getInstance();
		fxManager = GameObject.FindWithTag("SFX").GetComponent<FxManager>();
    }

    // Update is called once per frame
    void Update () {
		
	}

    public override void destroy()
    {
        gameObject.layer = LayerMask.NameToLayer("DeadEnnemiLayer");
        foreach (Transform child in transform)
        {
            child.gameObject.layer = LayerMask.NameToLayer("DeadEnnemiLayer");
        }
        GameObject.Destroy(gameObject.GetComponent<Collider>());
        Rigidbody[] rs = gameObject.GetComponentsInChildren<Rigidbody>();

        foreach (Rigidbody r in rs)
        {
            r.isKinematic = false;
            r.constraints = 0;
            UnityEngine.Object.Destroy(r.gameObject, 3);
        }
        Destroy(gameObject, 3);
        GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().gainExperince(ennemi.Experience);
	    fxManager.pickUp((ennemi as Chest).potion);
	    gameParameters.Hero.Inventory[(ennemi as Chest).potion]++;
    }


}
