﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dragonScript : EnnemiScript
{

    private int test = 0;
    private bool complete = false;
    private float gaucheParcourue;
    private float basParcourue;
    private bool aDroite;
    private GameObject player;

    // Use this for initialization
    void Start()
    {
        ennemi = new DragonWhelp();
        //Invoke("startRun", 1);
        aDroite = false;
        gaucheParcourue = 0;
        basParcourue = 0;
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        objectMovement(timeKeeper.timescale, timeKeeper.isPause());
        if (!timeKeeper.isPause())
        {


            if (gaucheParcourue >= 0.25)
            {
                aDroite = true;
            }
            else if (gaucheParcourue <= -0.25)
            {
                aDroite = false;
            }
            if (aDroite)
            {
                transform.Translate(Vector3.right * Time.deltaTime);
                gaucheParcourue -= Time.deltaTime;
            }
            else
            {
                transform.Translate(Vector3.left * Time.deltaTime);
                gaucheParcourue += Time.deltaTime;
            }
            if (basParcourue <= 15.25)
            {
                transform.Translate(Vector3.down * Time.deltaTime * 3);
                basParcourue += Time.deltaTime * 3;
            }
            if (player.transform.position.z - transform.position.z >= -2)
            {
                player.GetComponent<Player>().takeDamage(1);
            }
        }
    }

   
    public override void objectMovement(float timescale, bool isPause)
    {
        if (isPause)
        {
            GetComponent<Rigidbody>().isKinematic = true;
        }
        else
        {
            GetComponent<Rigidbody>().isKinematic = false;
        }
        if (!ennemi.Indestructible)
        {
            if (player.transform.position.z - transform.position.z >= -2)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, player.transform.position.z + 2);
                GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 5 * timescale);
            }
            else
            {
                GetComponent<Rigidbody>().velocity = new Vector3(0, GetComponent<Rigidbody>().velocity.y, 0);
            }
        }
        else
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0, GetComponent<Rigidbody>().velocity.y, 10);
        }

    }

    public override void startRun()
    {
        GetComponent<Rigidbody>().isKinematic = false;
    }

}
