﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fireball : MonoBehaviour
{
    public GameObject[] prefabs;
    private Transform playerTransform;
    private List<GameObject> fireballList;
    private List<Quaternion> fireballListRotation;
    
    Vector3 fireballSpawn = new Vector3(0.0f, 1.0f, 0.5f);
    public Vector3 fireEffectSpawn = new Vector3(0.0f, 0.2f, 0.3f);
    private string handType;
    public bool fireballPreparation = false;
	public float rx = 0f;
	public float ry = 0f;
	public float rz = 0f;
	public float mrx = 180f;
    public float mirx = 180f;
    public float srx = 1;
    public int genki = 0;
    public GameObject fireEffect;
    public int fireEffectIndex = 2;
    private TimeKeeper time = TimeKeeper.Instance;
    // Use this for initialization
    void Start ()
    {
        fireballList = new List<GameObject>();
        fireballListRotation = new List<Quaternion>();
        if (GameParameters.getInstance().MainHand == 0)
        {
            handType = "RightHand";
        }
        else
        {
            handType = "LeftHand";
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!time.isPause())
        {
            if (fireEffect)
            {
                GameObject rightHand = GameObject.FindGameObjectWithTag(handType);
                if (rightHand)
                {
                    playerTransform = rightHand.transform;
                    fireEffect.transform.position = playerTransform.position + fireEffectSpawn;
                }
            }
            if (Input.GetKeyDown(KeyCode.A))
            {
                randomFireball();
            }
        }
    }


    public void spawnObject(int prefabIndex)
    {
        if (!time.isPause())
        {
            if ((prefabIndex == 0 && fireballPreparation) || (genki == 2 && prefabIndex != 0))
            {
                Quaternion rota;
                GameParameters gameParameters = GameParameters.getInstance();
                if (gameParameters.Hero.Level >= 4)
                {
                    prefabIndex = 3;
                }
                GameObject go = Instantiate(prefabs[prefabIndex]) as GameObject;
                playerTransform = GameObject.FindGameObjectWithTag(handType).transform;
                go.transform.SetParent(transform);
                float rotax = Mathf.Max(mirx + playerTransform.rotation.y * 180 * srx, mrx);
                if (genki == 2 && prefabIndex != 0)
                {
                    rotax = 330;
                }
                rota = Quaternion.Euler(rotax + rx, 0, 0);

                if (playerTransform.position.x <= 0.8)
                {
                    fireballSpawn = new Vector3(-2.0f, 0.5f, 1.5f);
                }
                else if (playerTransform.position.x <= 1.2)
                {
                    fireballSpawn = new Vector3(0.0f, 0.5f, 1.5f);
                }
                else
                {
                    fireballSpawn = new Vector3(2.0f, 0.5f, 1.5f);
                }
                go.transform.position = playerTransform.position + fireballSpawn;
                go.transform.rotation = rota;
                go.GetComponent<DigitalRuby.PyroParticles.FireBaseScript>().damage = gameParameters.Hero.getDamage();
                fireballList.Add(go);
                fireballListRotation.Add(rota);
                fireballPreparation = false;
                if (fireEffect)
                {
                    Destroy(fireEffect);
                    fireEffect = null;
                }
                if (fireballList.Count > 12)
                {
                    Destroy(fireballList.ToArray()[0]);
                    fireballList.RemoveAt(0);
                    fireballListRotation.RemoveAt(0);
                }
                if (gameParameters.Hero.Level >= 5)
                {
                    randomFireball();
                }
                if (prefabIndex != 0 && prefabIndex != 3 && genki == 2)
                {
                    genki = 0;
                }
            }
        }
    }

    public void randomFireball()
    {
        GameParameters gameParameters = GameParameters.getInstance();
        Quaternion rota;
        float rotax = Mathf.Max(mirx + Random.value * 180 * srx, mrx);
        rota = Quaternion.Euler(rotax + rx, 0, 0);
        float random = Random.value*3;
        if (random <= 1)
        {
            fireballSpawn = new Vector3(-2.0f, 0.5f, 0.5f);
        }
        else if (random <= 2)
        {
            fireballSpawn = new Vector3(0.0f, 0.5f, 0.5f);
        }
        else
        {
            fireballSpawn = new Vector3(2.0f, 0.5f, 0.5f);
        }
        GameObject go = Instantiate(prefabs[3]) as GameObject;
        go.GetComponent<DigitalRuby.PyroParticles.FireBaseScript>().damage = gameParameters.Hero.getDamage();
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        go.transform.SetParent(transform);
        go.transform.position = playerTransform.position + fireballSpawn;
        go.transform.rotation = rota;
        fireballList.Add(go);
        fireballListRotation.Add(rota);


    }

    public void sendFireball()
    {
        if (!time.isPause())
        {
            Quaternion rota;
            prefabs[0].GetComponent<DigitalRuby.PyroParticles.FireBaseScript>().damage = 50;
            GameObject go = Instantiate(prefabs[0]) as GameObject;
            go.transform.SetParent(transform);
            rota = Quaternion.Euler(330 + rx, 0, 0);
            // rota = Quaternion.Euler(1, 0, 0);
            fireballSpawn = new Vector3(2.0f, 0.5f, 0.5f);
            playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
            go.transform.position = playerTransform.position + fireballSpawn;
            go.transform.rotation = rota;
            fireballList.Add(go);
            fireballListRotation.Add(rota);
            fireballPreparation = false;
            if (fireEffect)
            {
                Destroy(fireEffect);
                fireEffect = null;
            }
            if (fireballList.Count > 6)
            {
                Destroy(fireballList.ToArray()[0]);
                fireballList.RemoveAt(0);
                fireballListRotation.RemoveAt(0);
            }
        }
    }

    public void prepareFireball()
    {
        if (!time.isPause())
        {
            fireballPreparation = true;
            if (!fireEffect)
            {
                fireEffect = Instantiate(prefabs[fireEffectIndex]) as GameObject;
                GameObject playerRightHand = GameObject.FindGameObjectWithTag(handType);
                if (playerRightHand != null)
                {
                    playerTransform = playerRightHand.transform;
                    fireEffect.transform.SetParent(transform);
                    fireEffect.transform.position = playerTransform.position + fireEffectSpawn;
                }
            }
        }
    }

    public void charge1()
    {
        if (genki == 0)
        {
            genki = 1;
        }
    }

    public void charge2()
    {
        if (genki == 1)
        {
            genki = 2;
        }
    }
    
}
