﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fixeEnnemiSpawn : MonoBehaviour {

    public GameObject[] prefabs;
    public int lastSpawn;
    private int counter=0;
    private bool gauche;

	// Use this for initialization
	void Start () {
        InvokeRepeating("spawnEnnemi", 1.0f, 5.0f);
        gauche = true;
        
        /*
        int index = counter % 3;
		if(index == 1 || index == 2){
			Instantiate(prefabs[index], this.transform);
        }
		counter += 1;*/
    }
	
	// Update is called once per frame
	void Update () {

        

    }



    void spawnEnnemi()
    {
        if (counter < prefabs.Length && lastSpawn > counter)
        {

            GameObject go = Instantiate(prefabs[counter], transform.parent.gameObject.transform.position + new Vector3(0, 0, 30), new Quaternion());
            //Debug.Log(counter);

            if (prefabs[counter].tag == "Wall")
            {
                go.transform.Rotate(0, 30, 0);

            }
            if (prefabs[counter].tag == "Dragon")
            {
                if (gauche)
                {
                    go.transform.Translate(-2, 15, 0);
                    gauche = false;
                } else
                {
                    go.transform.Translate(-1.5f, 15, 0);
                    gauche = true;
                }


            }
            if (prefabs[counter].tag == "Canon")
            {
                go.transform.Translate(-5, -0.585f, 0);
                go.transform.Rotate(0, 120, 0);

            } 
            if (prefabs[counter].GetComponent<lancerScript>() != null || prefabs[counter].GetComponent<chestScript>() != null)
            {
                go.transform.Rotate(0, 180, 0);
            }
            counter++;
        }
    }
}
