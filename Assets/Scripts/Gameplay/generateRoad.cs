﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class generateRoad : MonoBehaviour {
    public  GameObject[] prefabs;
    private Transform playerTransform;
    private List<GameObject> roadList;
    private GameParameters gameParameters = GameParameters.getInstance();


    private int amountOfRoads = 2;
    private float spawnz      = 40f;
    private float roadLength  = 40f;
    private static int counter = 1;

    // Use this for initialization
    void Start () {
        roadList        = new List<GameObject>();
        playerTransform = GameObject.FindGameObjectWithTag("MainCamera").transform;
	}
	
	// Update is called once per frame
	void Update () {
		if (playerTransform.position.z > (spawnz - amountOfRoads * roadLength))
        {
            int index = counter % 2;
            spawnRoad(index);
            counter += 1;
            gameParameters.Score += 100;
        }
	}

    private void spawnRoad(int prefabIndex)
    {
        GameObject go = Instantiate(prefabs[prefabIndex]) as GameObject;
        go.transform.SetParent(transform);
        go.transform.position = Vector3.forward * spawnz;
        spawnz += roadLength;
        roadList.Add(go);

        if (roadList.Count > amountOfRoads + 1)
        {
            Destroy(roadList.ToArray()[0]);
            roadList.RemoveAt(0);
        }
    }
}
