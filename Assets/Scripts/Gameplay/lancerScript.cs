﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lancerScript : EnnemiScript {
    

	// Use this for initialization
	void Start () {
        ennemi = new Lancer();
        Invoke("startRun", 1);
    }
	
	// Update is called once per frame
	void Update ()
    {
        objectMovement(timeKeeper.timescale, timeKeeper.isPause());
        transform.position = new Vector3((Mathf.Clamp(transform.position.x, -1, 1)), transform.position.y, transform.position.z);

    }
}
