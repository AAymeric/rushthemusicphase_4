﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class manageTime : MonoBehaviour
{

	public AudioSource audio;
	public GameObject pausePanel;
	public GameObject winPanel;
	public Text greetingsText;
	public GameObject defeatPanel;
	public Text defeatText;
	public GameObject endPanel;
	public TimeKeeper timeKeeper;
	public GameObject player;
	public GameObject portal;
	private GameObject portalClone;
	private GameParameters parameters;
	
	// Use this for initialization
	void Start ()
	{
		audio = GetComponent<AudioSource>();
		timeKeeper = TimeKeeper.Instance;
		parameters = GameParameters.getInstance();
	}
	
	// Update is called once per frame
	void Update ()
	{
		float variation = (audio.clip.length * 5) / 100;
		if (audio.time > (audio.clip.length - 15) && GameObject.Find(portal.name) == null)
		{
			Vector3 portalPosition = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z + (10 * Mathf.Max(player.GetComponent<Rigidbody>().velocity.z,5)));
			portal = Instantiate(portal, portalPosition, player.transform.rotation);
			showEndMessage();
			Invoke("hideEndMessage", 3.0f);
		}

		if (Input.GetKeyDown(KeyCode.Space))
		{
			showPauseMenu();
		}
	}

	public void showPauseMenu()
	{
		timeKeeper.pause();
		if (audio.isPlaying)
		{
			audio.Pause();
			pausePanel.SetActive(true);
		}
		else
		{
			audio.UnPause();
			pausePanel.SetActive(false);
		}
	}

	public void showWinMenu()
	{
		timeKeeper.forcePause();
		greetingsText.text += " " + parameters.LevelNumber + " !";
		winPanel.SetActive(true);
		GameParameters.getInstance().save();
	}

	public void hideWinMenu()
	{
		timeKeeper.unPause();
		Debug.Log(parameters.LevelNumber);
		winPanel.SetActive(false);
	}
	
	public void showDefeatMenu()
	{
	    timeKeeper.forcePause();
		defeatText.text += " " + parameters.LevelNumber + "...";
		defeatPanel.SetActive(true);
		GameParameters.getInstance().Score = PlayerPrefs.GetInt("score");
	}

	public void hideDefeatMenu()
	{
		timeKeeper.unPause();
		defeatPanel.SetActive(false);
	}

	public void showEndMessage()
	{
		endPanel.SetActive(true);
	}

	public void hideEndMessage()
	{
		endPanel.SetActive(false);
	}
}
