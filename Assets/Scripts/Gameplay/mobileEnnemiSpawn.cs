using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mobileEnnemiSpawn : MonoBehaviour {

    public List<GameObject> prefabs;
    private static int counter=0;

	// Use this for initialization
	void Start () {
		int index = counter % 4;
		if(index != 0){
			Instantiate(prefabs[index], this.transform);
		}
        Debug.Log("index = " + index);
        Debug.Log("counter = " + counter);
        counter += 1;
    }
	 
	// Update is called once per frame
	void Update () {
		
	}
}
