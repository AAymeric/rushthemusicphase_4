﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class portalScript : MonoBehaviour
{

	private GameObject manager;
	private TimeKeeper timekeeper;
	
	// Use this for initialization
	void Start () {
		manager = GameObject.FindWithTag("AudioManager");
		timekeeper = TimeKeeper.Instance;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void OnTriggerEnter(Collider col)
	{
		if (col.CompareTag("Player") && !timekeeper.isPause())
		{
			manager.GetComponent<manageTime>().showWinMenu();
            GameObject[] ennemis = GameObject.FindGameObjectsWithTag("Ennemi");
            for (int i = ennemis.Length - 1; i >= 0; i--)
            {
                Destroy(ennemis[i]);
            }
            GameObject[] dragons = GameObject.FindGameObjectsWithTag("Dragon");
            for (int i = dragons.Length - 1; i >= 0; i--)
            {
                Destroy(dragons[i]);
            }

        }
	}
}
