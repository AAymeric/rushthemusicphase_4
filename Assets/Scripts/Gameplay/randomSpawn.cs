﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class randomSpawn : MonoBehaviour {

    public bool spawning;
    private TimeKeeper time = TimeKeeper.Instance;
    public GameObject[] prefabs;
    private int spawnedCanon; // TODO: Vérifier qu'il n'y ait pas trop de canon spawn en même temps 
    private float spawnTimer;
    private float timeBeforeSpawn;
    private bool gaucheD;
    private bool gaucheC;


    // Use this for initialization
    void Start () {
        spawning = true;
        spawnedCanon = 0;
        spawnTimer = 0;
        timeBeforeSpawn = 3-GameParameters.getInstance().LevelNumber/50;
        gaucheC = true;
        gaucheD = false;

    }
	
	// Update is called once per frame
	void Update () {
        if(spawning && !time.isPause())
        {
            spawnTimer+= Time.deltaTime;
            if(spawnTimer>= timeBeforeSpawn)
            {
                spawnTimer = 0;
                float spawning = Random.Range(0, prefabs.Length);
                int spawn = Mathf.FloorToInt(spawning);
                if (spawning > 5.5f)
                {
                    spawn = 4;
                }
                GameObject go = Instantiate(prefabs[spawn], transform.parent.gameObject.transform.position + new Vector3(0, 0, 30), new Quaternion());
                
                if (prefabs[spawn].tag == "Wall")
                {
                    go.transform.Rotate(0, 30, 0);

                }
                if (prefabs[spawn].tag == "Dragon")
                {
                    if (gaucheD)
                    {
                        go.transform.Translate(-2, 15, 0);
                        gaucheD = false;
                    }
                    else
                    {
                        go.transform.Translate(-1.5f, 15, 0);
                        gaucheD = true;
                    }
                }
                if (prefabs[spawn].GetComponent<lancerScript>() != null || prefabs[spawn].GetComponent<chestScript>() != null)
                {
                    go.transform.Rotate(0, 180, 0);
                }
                if (prefabs[spawn].tag == "Canon")
                {
                    if (gaucheC)
                    {
                        go.transform.Translate(-5, -0.585f, 55);
                        go.transform.Rotate(0, 120, 0);
                        gaucheC = false;
                    }
                    else
                    {
                        go.transform.Translate(+5, -0.585f, 55);
                        go.transform.Rotate(0, 180, 0);
                        gaucheC = true;
                    }
                }
            } 
        }
		
	}

    public void stopSpawn()
    {
        spawning = false;
    }
}
