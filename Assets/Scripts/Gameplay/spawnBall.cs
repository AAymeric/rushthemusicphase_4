﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnBall : MonoBehaviour
{

	public GameObject gBall;
	public GameObject gBox;
	public bool spawn = true;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		/*if (spawn)
		{
			Invoke("spawnB", 1.0f);
			spawn = false;
		}*/
		
		if (spawn)
		{
			Invoke("spawnBox", 2.0f);
			spawn = false;
		}
	}

	private void spawnB()
	{
		spawn = true;
		GameObject ball = Instantiate(gBall);
		ball.AddComponent<Rigidbody>();
		ball.GetComponent<Rigidbody>().velocity   = new Vector3(0, 0, -5);
		ball.GetComponent<Rigidbody>().useGravity = false;
		Destroy(ball, 10.0f);
	}
	
	private void spawnBox()
	{
		spawn = true;
		GameObject box = Instantiate(gBox);
		box.AddComponent<Rigidbody>();
		box.GetComponent<Rigidbody>().velocity   = new Vector3(0, 0, -5);
		box.GetComponent<Rigidbody>().useGravity = false;
		Vector3 position = box.transform.position;
		position.y += 0.6f;
		box.transform.position = position;
		Destroy(box, 10.0f);
	}

}
