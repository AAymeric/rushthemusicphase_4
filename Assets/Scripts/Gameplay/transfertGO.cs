﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class transfertGO : MonoBehaviour
{

	public GameParameters gameParametersInstance;

	// Use this for initialization
	void Start ()
	{
		this.gameParametersInstance = GameParameters.getInstance();
		this.gameParametersInstance.Hero = new Monk();
		Invoke("change", 3);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void change()
	{
		SceneManager.LoadScene("Weapon");
	}
}
