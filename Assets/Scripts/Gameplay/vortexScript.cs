﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vortexScript : MonoBehaviour {
    public Texture[] frame;
    int framesPerSecond = 60;

    bool prepa = false;
    bool spawn = false;
    float timeP;
    float timeS;
    string handType;

    // Use this for initialization
    void Start ()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        transform.position = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z - 1);
        if (GameParameters.getInstance().MainHand != 0)
        {
            handType = "RightHand";
        }
        else
        {
            handType = "LeftHand";
        }

    }
	
	// Update is called once per frame
	void Update ()
    {
        int index = (int)(Time.time * framesPerSecond) % frame.Length;
        GetComponent<Renderer>().material.mainTexture = frame[index];

        if (Input.GetKeyDown(KeyCode.T)  && GameParameters.getInstance().Hero is Magician)
        {
            prepareVortex();
        }
        if (Input.GetKeyDown(KeyCode.Y) && GameParameters.getInstance().Hero is Magician)
        {
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            transform.position = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z + 1);
            spawnVortex();
        }

        if (prepa)
        {
            float size = Time.time - timeP;
            transform.localScale = new Vector3(size*0.4f, 0.00001f, size*0.4f);
            Hero hero = GameParameters.getInstance().Hero;

            hero.CurrentEnergy = Mathf.Max(0, (int)(hero.MaxEnergy - size * 0.4 * 1000));
            if (hero.MaxEnergy < size* 0.4 * 1000)
            {
                spawnVortex();
            }
        }

        if (spawn)
        {
            float tempsDepuisCreation = Time.time - timeS;
            float tailleInitiale = timeS - timeP;
            float size = tailleInitiale - Mathf.Max(0, (tempsDepuisCreation-1)* tailleInitiale);
            if (size > 0)
            {
                transform.localScale = new Vector3(size * 0.4f, 0.00001f, size * 0.4f);
            }
            if(tempsDepuisCreation > 2)
            {
                GameObject player = GameObject.FindGameObjectWithTag("Player");
                transform.position = new Vector3 (player.transform.position.x, player.transform.position.y, player.transform.position.z - 1);
                spawn = false;
                Hero hero = GameParameters.getInstance().Hero;
                hero.CurrentEnergy = hero.MaxEnergy;

                transform.localScale = new Vector3(0, 0.00001f, 0);
            }
        }

    }


    public void prepareVortex()
    {
        if(!prepa && !spawn)
        {
            GameObject hand = GameObject.FindGameObjectWithTag(handType);
            if (hand)
            {
                transform.position = hand.transform.position + new Vector3(0,0,0.5f);
            }
            else
            {
                GameObject player = GameObject.FindGameObjectWithTag("Player");
                transform.position = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z + 1);
            }
            timeP = Time.time;
            prepa = true;
        }
    }

    public void spawnVortex()
    {
        if(!spawn && prepa)
        {
            spawn = true;
            prepa = false;
            timeS = Time.time;
        }
    }
}
