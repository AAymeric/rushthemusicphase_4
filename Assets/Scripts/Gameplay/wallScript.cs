﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wallScript : EnnemiScript
{



    // Use this for initialization
    void Start()
    {
        ennemi = new Wall(100, 100, 10, new Ranged(), false, null, 10.0f);
    }

    // Update is called once per frame
    void Update()
    {

    }



    public void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Weapon"))
        {
            if (!this.ennemi.Indestructible)
            {
                takeDamage(50);
            }
        }
    }

    public override void destroy()
    {

        gameObject.layer = LayerMask.NameToLayer("DeadEnnemiLayer");
        foreach (Transform child in transform)
        {
            child.gameObject.layer = LayerMask.NameToLayer("DeadEnnemiLayer"); 
        }
        GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().gainExperince(ennemi.Experience);
        GameObject.Destroy(gameObject.GetComponent<Collider>());
        //GetComponent<Rigidbody>().velocity = new Vector3(0, -5, 0);
        //UnityEngine.Object.Destroy(gameObject, 5);
        Rigidbody[] rs = gameObject.GetComponentsInChildren<Rigidbody>();

        foreach (Rigidbody r in rs)
        {
            if (r.CompareTag("Wall"))
            {
                r.isKinematic = false;
                UnityEngine.Object.Destroy(r.gameObject, 3);
            }
            if (r.CompareTag("WallErase"))
            {
                UnityEngine.Object.Destroy(r.gameObject);
            }
        }
    }
}
