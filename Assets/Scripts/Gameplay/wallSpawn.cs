﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wallSpawn : MonoBehaviour {

    public List<GameObject> prefabs;
    private static int counter = 0;

    // Use this for initialization
    void Start()
    {

        int index = counter % 2;
        if (index == 1)
        {
            Instantiate(prefabs[index], this.transform);
        }
        counter += 1;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
